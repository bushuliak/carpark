<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');

//>Админка
$adminGroupData = [
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'is_admin'],
];

Route::group($adminGroupData, function () {
    $methods = ['index', 'create', 'edit'];
    Route::resource('cars', CarController::class, ['only' => $methods])
        ->names('admin.cars');
});

Route::group($adminGroupData, function () {
    $methods = ['index', 'create', 'edit'];
    Route::resource('parks', ParkController::class, ['only' => $methods])
        ->names('admin.parks');
});

Route::group($adminGroupData, function () {
    $methods = ['index', 'create', 'edit'];
    Route::resource('users', UserController::class, ['only' => $methods])
        ->names('admin.users');
});
//<

Route::group(['namespace' => 'User', 'middleware' => ['auth']], function () {
    $methods = ['index', 'create', 'edit'];
    Route::resource('cars', CarController::class, ['only' => $methods])
        ->names('cars');
});
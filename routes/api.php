<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//>Админка
Route::middleware(['auth:api', 'is_admin'])
    ->apiResource('admin/cars', Api\Admin\CarController::class);

Route::middleware(['auth:api', 'is_admin'])
    ->apiResource('admin/parks', Api\Admin\ParkController::class);

Route::middleware(['auth:api', 'is_admin'])
    ->apiResource('admin/users', Api\Admin\UserController::class);
//<

Route::middleware(['auth:api'])
    ->apiResource('user/cars', Api\User\CarController::class);
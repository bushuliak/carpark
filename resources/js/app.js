require('./bootstrap');
require('../../node_modules/bootstrap-select/dist/js/bootstrap-select');

window.Vue = require('vue');

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//>Components
Vue.component('header-component', require('./components/includes/HeaderComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
//<

//>Admin
Vue.component('admin-cars-content', require('./components/admin/cars/CarsContentComponent.vue').default);
Vue.component('admin-car-card', require('./components/admin/cars/CarCardComponent.vue').default);
Vue.component('admin-create-car-form', require('./components/admin/cars/CreateCarComponent.vue').default);
Vue.component('admin-edit-car-form', require('./components/admin/cars/EditCarComponent.vue').default);

Vue.component('admin-parks-content', require('./components/admin/parks/ParksContentComponent.vue').default);
Vue.component('admin-park-card', require('./components/admin/parks/ParkCardComponent.vue').default);
Vue.component('admin-create-park-form', require('./components/admin/parks/CreateParkComponent.vue').default);
Vue.component('admin-edit-park-form', require('./components/admin/parks/EditParkComponent.vue').default);

Vue.component('admin-users-content', require('./components/admin/users/UsersContentComponent.vue').default);
Vue.component('admin-user-card', require('./components/admin/users/UserCardComponent.vue').default);
Vue.component('admin-create-user-form', require('./components/admin/users/CreateUserComponent.vue').default);
Vue.component('admin-edit-user-form', require('./components/admin/users/EditUserComponent.vue').default);

Vue.component('admin-users-content', require('./components/admin/users/UsersContentComponent.vue').default);
Vue.component('admin-user-card', require('./components/admin/users/UserCardComponent.vue').default);
Vue.component('admin-create-user-form', require('./components/admin/users/CreateUserComponent.vue').default);
Vue.component('admin-edit-user-form', require('./components/admin/users/EditUserComponent.vue').default);
//<

//>User
Vue.component('cars-content', require('./components/cars/CarsContentComponent.vue').default);
Vue.component('car-card', require('./components/cars/CarCardComponent.vue').default);
Vue.component('create-car-form', require('./components/cars/CreateCarComponent.vue').default);
Vue.component('edit-car-form', require('./components/cars/EditCarComponent.vue').default);
//<

const app = new Vue({
    el: '#app',
});

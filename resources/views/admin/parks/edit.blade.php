@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Edit park')

@section('content')
    <admin-edit-park-form
        park-json="{{ $park }}"
        cars-json="{{ $cars }}">
    </admin-edit-park-form>
@endsection

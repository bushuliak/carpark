@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Create park')

@section('content')
    <admin-create-park-form
        cars-json="{{ $cars }}">
    </admin-create-park-form>
@endsection

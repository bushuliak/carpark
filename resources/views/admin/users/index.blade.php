@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Users')

@section('content')
    <admin-users-content></admin-users-content>
@endsection
@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Cars')

@section('content')
    <admin-cars-content></cars-content>
@endsection
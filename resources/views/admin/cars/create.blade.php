@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Create car')

@section('content')
    <admin-create-car-form
        users-json="{{ $users }}"
        parks-json="{{ $parks }}">
    </admin-create-car-form>
@endsection

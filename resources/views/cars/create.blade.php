@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Create car')

@section('content')
    <create-car-form
        parks-json="{{ $parks }}">
    </create-car-form>
@endsection

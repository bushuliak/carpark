@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Edit car')

@section('content')
    <edit-car-form
        car-json="{{ $car }}"
        parks-json="{{ $parks }}">
    </edit-car-form>
@endsection

@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Cars')

@section('content')
    <cars-content></cars-content>
@endsection
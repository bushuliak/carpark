<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StorePark extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->isAdmin();
    }
    /**
     * Get message if validation fails
     *
     * @return array
    */
    public function messages()
    {
        return [
            'name.required' => 'Введите, пожалуйста, название автопарка',
            'address.unique' => 'Введите, пожалуйста, адрес автопарка',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required'],
            'address' => ['bail', 'required'],
            'schedule' => ['nullable'],
        ];
    }
}

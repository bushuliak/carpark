<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->isAdmin();
    }
    /**
     * Get message if validation fails
     * 
     * @return array
    */
    public function messages()
    {
        return [
            'name.required' => 'Введите, пожалуйста, имя пользователя',
            'name.min' => 'Имя пользователя не может содержать менее 2 символов',
            'email.required' => 'Введите, пожалуйста, Email пользователя',
            'email.unique' => 'Пользователь с таким Email уже зарегистрирован',
            'password.required' => 'Введите, пожалуйста, пароль пользователя',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'min:2'],
            'email' => ['bail', 'required', 'unique:users'],
            'password' => ['bail', 'required'],
            'is_admin' => ['nullable'],
            'cars_selected' => ['nullable'],
        ];
    }
}

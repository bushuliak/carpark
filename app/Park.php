<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'address', 'schedule'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    /**
     * Get park cars.
     *
     */
    public function cars()
    {
        return $this->belongsToMany(Car::class,
            'car_park', 'park_id', 'car_id');
    }
}

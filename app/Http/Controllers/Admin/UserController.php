<?php

namespace App\Http\Controllers\Admin;

use App\Car;
use App\User;
use App\Http\Resources\Admin\UserResource;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create', [
            'cars' => Car::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = new UserResource($user);

        return view('admin.users.edit', [
            'user' => $user->toJson(),
            'cars' => Car::all(),
        ]);
    }
}

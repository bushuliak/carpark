<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Car;
use App\Park;
use App\Http\Resources\Admin\CarResource;

class CarController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.cars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cars.create', [
            'users' => User::all(),
            'parks' => Park::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        $car = new CarResource($car);

        return view('admin.cars.edit', [
            'car' => $car->toJson(),
            'users' => User::all(),
            'parks' => Park::all(),
        ]);
    }
}

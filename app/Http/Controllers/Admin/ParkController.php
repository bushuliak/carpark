<?php

namespace App\Http\Controllers\Admin;

use App\Car;
use App\Park;
use App\Http\Resources\Admin\ParkResource;

class ParkController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parks.create', [
            'cars' => Car::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Park $park)
    {
        $park = new ParkResource($park);

        return view('admin.parks.edit', [
            'park' => $park->toJson(),
            'cars' => Car::all(),
        ]);
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    /**
     * Explode or return empty array
     * 
     * @return array
     */
    public function explode($char, $string = '')
    {
        return preg_split('@,@', $string, NULL, PREG_SPLIT_NO_EMPTY);
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Car;
use App\Park;
use App\Http\Resources\User\CarResource;

class CarController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create', [
            'parks' => Park::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        if (count($car->users->where('id', \Auth::id())) > 0) {
            $car = new CarResource($car);

            return view('cars.edit', [
                'car' => $car->toJson(),
                'parks' => Park::all(),
            ]);
        }

        return redirect()->route('index');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    /**
     * Explode or return empty array
     * 
     * @return array
     */
    public function explode($char, $string = '')
    {
        return preg_split('@,@', $string, NULL, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Sync relationships
     * 
     * @return void
     */
    protected function syncOrDetach($obj, $pivot, $selected)
    {
        (!empty($selected)) ? $obj->$pivot()->sync($selected) : $obj->$pivot()->detach();
    }

}

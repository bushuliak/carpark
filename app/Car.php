<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'car_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    /**
     * Get car drivers.
     *
     */
    public function users()
    {
        return $this->belongsToMany(User::class,
            'car_user', 'car_id', 'user_id');
    }

    /**
     * Get car parks.
     *
     */
    public function parks()
    {
        return $this->belongsToMany(Park::class,
            'car_park', 'car_id', 'park_id');
    }
}

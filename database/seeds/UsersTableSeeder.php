<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $users = [[
            'name'       => 'Администратор',
            'email'      => 'admin@mail.com',
            'password'   => Hash::make('password'),
            'is_admin'   => 1,
            'created_at' => $faker->dateTime(),
            'updated_at' => $faker->dateTime(),
        ]];

        for ($i = 1; $i <= 10; $i++) {
            $users[] = [
                'name'       => $faker->name,
                'email'      => "user$i@mail.com",
                'password'   => Hash::make("password$i"),
                'is_admin'   => 0,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ];
        }

        \DB::table('users')->insert($users);
    }
}

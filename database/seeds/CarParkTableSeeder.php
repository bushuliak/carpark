<?php

use Illuminate\Database\Seeder;

class CarParkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        for ($i = 1; $i <= 10; $i++) {
            $data[] = [
                'car_id' => rand(2, 20),
                'park_id' => rand(2, 10),
            ];
        }

        \DB::table('car_park')->insert($data);
    }
}

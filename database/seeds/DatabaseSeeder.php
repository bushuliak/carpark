<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CarsTableSeeder::class);
        $this->call(ParksTableSeeder::class);
        $this->call(CarUserTableSeeder::class);
        $this->call(CarParkTableSeeder::class);
    }
}
